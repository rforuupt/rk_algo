package com.rk.graph;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestMinimalTree_4_2 {

	MinimalTree_4_2 minimalTree;
	int []arr;
	@Before
	public void setUp() throws Exception {
		minimalTree = new MinimalTree_4_2();
		arr = new int[]{1, 2, 3};
	}

	@Test
	public void testCreateMinimalTree() {
		BSTNode root = minimalTree.createMinimalTree(arr);
		assertEquals(2, root.value);
		assertEquals(1, root.left.value);
	}

}
