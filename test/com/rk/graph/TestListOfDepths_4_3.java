package com.rk.graph;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TestListOfDepths_4_3 {

	ListOfDepths_4_3 listOfDepths;
	BSTNode root;
	
	@Before
	public void setUp() throws Exception {
		listOfDepths = new ListOfDepths_4_3();
		
	}

	@Test
	public void testEmptyListForNull() {
		List<LinkedList<BSTNode>> llList = listOfDepths.getListOfDepths(null);
		assertTrue(llList.isEmpty());
	}
	
	@Test
	public void sizeShouldBeOne() {
		root = new BSTNode(2);
		List<LinkedList<BSTNode>> llList = listOfDepths.getListOfDepths(root);
		assertTrue("size Should Be One",llList.size() ==1);
		assertTrue("value Should Be 2",llList.get(0).getFirst().value ==2);
	}
	
	@Test
	public void test3Nodes() {
		root = new BSTNode(2);
		root.left = new BSTNode(1);
		root.right = new BSTNode(3);
		List<LinkedList<BSTNode>> llList = listOfDepths.getListOfDepths(root);
		assertTrue("size Should Be 2",llList.size() ==2);
		assertTrue("value Should Be 2",llList.get(0).getFirst().value ==2);
		assertTrue("value Should Be 1",llList.get(1).getFirst().value ==1);
	}

}
