package com.rk.graph;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class TestRouteBetweenNodes_4_1 {
	
	RouteBetweenNodes_4_1 routeBetweenNodes;
	Graph graph;
	@Before
	public void setUp() throws Exception {
		routeBetweenNodes = new RouteBetweenNodes_4_1();
		graph = new Graph();
		Node a = new Node("A");
		Node b = new Node("B");
		Node c = new Node("C");
		a.children.add(b);
		b.children.add(a);
		graph.nodes.add(a);
		graph.nodes.add(b);
		graph.nodes.add(c);
	}

	@Test
	public void testTwoNodes() {
		RouteBetweenNodes_4_1 routeBetweenNodes = new RouteBetweenNodes_4_1();
		assertTrue("should return true as they have path",routeBetweenNodes.hasRoute(graph.nodes.get(0), graph.nodes.get(1)));
		assertFalse("should return false as they have no path",routeBetweenNodes.hasRoute(graph.nodes.get(0), null));
		assertFalse("should return false as they have no path",routeBetweenNodes.hasRoute(null, graph.nodes.get(2)));
		assertFalse("should return false as they have no path",routeBetweenNodes.hasRoute(graph.nodes.get(0), graph.nodes.get(2)));
	}

}
