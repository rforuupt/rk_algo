package com.rk.linkedlist;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import org.junit.Before;
import org.junit.Test;

public class TestLoopDetection {
	
	LoopDetection_2_8 loopDetector;
	Node<Integer> node;

	@Before
	public void setUp() throws Exception {
		loopDetector = new LoopDetection_2_8();
		node = new Node<Integer>(1);
	}

	@Test
	public void testNullNode() {
		assertNull("Should be null", loopDetector.getStartOfLoop(null));
	}
	
	@Test
	public void testSingleNode() {
		assertNull("Should be null", loopDetector.getStartOfLoop(node));
		node.next = node;
		assertSame(node, loopDetector.getStartOfLoop(node));
	}
	// 1>2>1
	@Test
	public void testTwoNode() {
		node.next = new Node<Integer>(2);
		assertNull("Should be null", loopDetector.getStartOfLoop(node));
		node.next.next = node;
		assertSame(node, loopDetector.getStartOfLoop(node));
	}
	
	@Test
	public void testTwoNodeSelf() {
		node.next = new Node<Integer>(2);
		node.next.next = node.next;
		assertSame(node.next, loopDetector.getStartOfLoop(node));
	}
	
	@Test
	public void testThreeNode() {
		node.next = new Node<Integer>(2);
		node.next.next = new Node<Integer>(3);
		node.next.next.next =node.next;
		assertSame(node.next, loopDetector.getStartOfLoop(node));
	}

}
