package com.rk.stackQueue;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StackMinTest {

	StackMin_3_2 stackMin;
	@Before
	public void setUp() throws Exception {
		stackMin = new StackMin_3_2();
	}

	@Test
	public void testPushPopAndPeek() {
		stackMin.push(1);
		assertEquals(1, stackMin.peek());
		assertEquals(1, stackMin.pop());
	}
	
	@Test
	public void testMin() {
		stackMin.push(3);
		stackMin.push(2);
		stackMin.push(2);
		stackMin.push(1);
		assertEquals(1, stackMin.min());
		stackMin.pop();
		assertEquals(2, stackMin.min());
		stackMin.pop();
		assertEquals(2, stackMin.min());
	}

}
