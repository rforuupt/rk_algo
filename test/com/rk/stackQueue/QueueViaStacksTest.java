package com.rk.stackQueue;

import static org.junit.Assert.*;

import org.junit.Test;

public class QueueViaStacksTest {

	@Test
	public void test() {
		QueueViaStacks_3_4 myQueue = new QueueViaStacks_3_4();
		myQueue.enque(1);
		myQueue.enque(2);
		assertEquals(1, myQueue.dequeue());
		assertEquals(2, myQueue.dequeue());
	}

}
