package com.rk.stackQueue;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SortStackTest {

	SortStack_3_5 stackOfPlates = new SortStack_3_5();
	
	@Test
	public void test() {
		stackOfPlates.push(2);
		stackOfPlates.push(3);
		stackOfPlates.push(1);
		stackOfPlates.push(6);
		assertEquals(1, stackOfPlates.pop());
		assertEquals(2, stackOfPlates.pop());
		
	}

}
