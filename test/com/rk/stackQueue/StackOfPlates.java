package com.rk.stackQueue;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StackOfPlates {

	StackOfPlates_3_3 stackOfPlates;
	
	@Before
	public void setUp() throws Exception {
		stackOfPlates = new StackOfPlates_3_3();
	}

	@Test
	public void test() {
		for(int i=1; i<20; i++){
			stackOfPlates.push(i);
		}
		
		assertEquals(19, stackOfPlates.pop());
		assertEquals(10, stackOfPlates.pop(1));
		assertEquals(9, stackOfPlates.pop(1));
		assertEquals(8, stackOfPlates.pop(1));
	}

}
