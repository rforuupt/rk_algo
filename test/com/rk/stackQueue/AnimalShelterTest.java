package com.rk.stackQueue;

import static org.junit.Assert.*;

import org.junit.Test;

public class AnimalShelterTest {

	@Test
	public void testDequeueAny() {
		AnimalShelter_3_6 animalShelter = new AnimalShelter_3_6();
		animalShelter.enqueue("dog1");
		animalShelter.enqueue("cat1");
		animalShelter.enqueue("dog2");
		animalShelter.enqueue("cat2");
		assertTrue("dog1".equals(animalShelter.dequeueAny()));
		assertTrue("cat1".equals(animalShelter.dequeueAny()));
	}

	@Test
	public void testDequeueCat() {
		AnimalShelter_3_6 animalShelter = new AnimalShelter_3_6();
		animalShelter.enqueue("dog1");
		animalShelter.enqueue("cat1");
		animalShelter.enqueue("dog2");
		animalShelter.enqueue("cat2");
		assertTrue("cat1".equals(animalShelter.dequeueCat()));
		assertTrue("dog1".equals(animalShelter.dequeueDog()));
	}
}
