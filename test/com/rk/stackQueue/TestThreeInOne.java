package com.rk.stackQueue;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.rk.stackQueue.ThreeInOne_3_1.StackOperationException;

public class TestThreeInOne {

	ThreeInOne_3_1 threeInOne;

	@Test
	public void testInvalidSizeOfArrays() {
		threeInOne = new ThreeInOne_3_1(0);
		try {
			threeInOne.push(1, 20);
			fail("Expected Exception");
		} catch (StackOperationException e) {
			assertTrue("Stack is Full".equalsIgnoreCase(e.getMessage()));
		}
	}
	
	@Test
	public void testSizeOneOfArrays() {
		threeInOne = new ThreeInOne_3_1(1);
			try {
				threeInOne.push(1, 10);
				Assert.assertEquals(10, threeInOne.pop(1));
				
				threeInOne.push(2, 20);
				Assert.assertEquals(20, threeInOne.pop(2));
				
				threeInOne.push(3, 30);
				Assert.assertEquals(30, threeInOne.pop(3));
			} catch (StackOperationException e) {
				e.printStackTrace();
			}
	}
	
	@Test
	public void testSizeOneOfArraysWithTwoPush() {
		threeInOne = new ThreeInOne_3_1(1);
			try {
				threeInOne.push(1, 10);
				threeInOne.push(1, 100);
			} catch (StackOperationException e) {
				assertTrue("Stack is Full".equalsIgnoreCase(e.getMessage()));
			}
	}
	
	@Test
	public void testSizeOneOfArraysWithTwoPush2() {
		threeInOne = new ThreeInOne_3_1(1);
			try {
				threeInOne.push(2, 10);
				threeInOne.push(2, 100);
			} catch (StackOperationException e) {
				assertTrue("Stack is Full".equalsIgnoreCase(e.getMessage()));
			}
	}
	
	@Test
	public void testSizeOneOfArraysWithTwoPush3() {
		threeInOne = new ThreeInOne_3_1(1);
			try {
				threeInOne.push(3, 10);
				threeInOne.push(3, 100);
			} catch (StackOperationException e) {
				assertTrue("Stack is Full".equalsIgnoreCase(e.getMessage()));
			}
	}
	
	@Test
	public void testSizeOneOfArraysWithTwoPush4() {
		threeInOne = new ThreeInOne_3_1(1);
			try {
				threeInOne.push(1, 10);
				threeInOne.push(3, 100);
			} catch (StackOperationException e) {
				assertTrue("Stack is Full".equalsIgnoreCase(e.getMessage()));
			}
	}
	
	@Test
	public void testSizeOneOfArraysTwoPop() {
		threeInOne = new ThreeInOne_3_1(1);
			try {
				threeInOne.push(1, 10);
				threeInOne.pop(3);
				threeInOne.pop(3);
			} catch (StackOperationException e) {
				assertTrue("Stack is Empty".equalsIgnoreCase(e.getMessage()));
			}
	}
	@Test
	public void testSizeOneOfArraysTwoPop2() {
		threeInOne = new ThreeInOne_3_1(1);
			try {
				threeInOne.push(1, 10);
				threeInOne.pop(3);
			} catch (StackOperationException e) {
				assertTrue("Stack is Empty".equalsIgnoreCase(e.getMessage()));
			}
	}
	
	@Test
	public void testSize3OfArraysWithTwoPush4() {
		threeInOne = new ThreeInOne_3_1(3);
			try {
				threeInOne.push(1, 10);
				threeInOne.push(2, 20);
				threeInOne.push(3, 30);
				Assert.assertEquals(10, threeInOne.pop(1));
				Assert.assertEquals(20, threeInOne.pop(2));
				Assert.assertEquals(30, threeInOne.pop(3));
			} catch (StackOperationException e) {
				fail(e.getMessage());
			}
	}
	
}
