package com.rk.linkedlist;

/*
 Implement an algorithm to delete a node in the middle (i.e., any node but the first and last node, not necessarily the 
 exact middle) of a singly linked list, given only access to that node.
EXAMPLE
Input: the node c from the linked list a - >b- >c - >d - >e- >f
Result: nothing is returned, but the new linked list looks like a - >b- >d - >e- >f
 */
public class DeleteMiddleNode_2_3 {

	public static void main(String[] args) {
		Node<Integer> n = new Node<>(1);
		for(int i=2; i<10; i++){
			n.appendToTail(i);
		}
		Node<Integer> k =n;
		for(int i=1; i<4; i++){
			k = k.next;
		}
		deleteMiddle(k);
		while(n!=null){
			System.out.println(n.data);
			n = n.next;
		}
	}
	public static void deleteMiddle(Node<Integer> n) {
		if(n==null || n.next==null){
			return;
		}else{
			n.data = n.next.data;
			n.next = n.next.next;
		}
			
	}

}
