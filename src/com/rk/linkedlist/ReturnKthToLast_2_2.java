package com.rk.linkedlist;

/*
 Implement an algorithm to find the kth to last element of a singly linked list.
 */
public class ReturnKthToLast_2_2 {

	public static void main(String[] args) {
		Node<Integer> n = new Node<>(1);
		for(int i=2; i<10; i++){
			n.appendToTail(i);
		}
		System.out.println(kthElenent(n, 9).data);
	}
	
	public static Node<Integer> kthElenent(Node<Integer> n, int k) {

		Node<Integer> slow = n;
		Node<Integer> fast = n;
		if(n==null || k<0)
			return null;
		for(int i=1; i<=k; i++){
			if(fast == null){
				return null ;
			}else{
				fast = fast.next;
			}
				
		}
		if(fast==null)
			return null;
		
		while(fast.next !=null){
			fast = fast.next;
			slow = slow.next;
		}
		
		return slow;
		
	}

}
