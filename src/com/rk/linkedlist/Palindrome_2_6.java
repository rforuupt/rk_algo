package com.rk.linkedlist;

import java.util.Stack;

/*
 *Implement a function to check if a linked list is a palindrome.
 */

public class Palindrome_2_6 {

	public static void main(String[] args) {
		Node<Integer> n = new Node<>(3);
		n.appendToTail(5);
		n.appendToTail(8);
		n.appendToTail(5);
		n.appendToTail(3);
		System.out.println(isPalindrom(n));
	}
	static boolean isPalindrom(Node<Integer> node){
		if(node == null || node.next == null){
			return true;
		}
		
		Node<Integer> slow = node;
		Node<Integer> fast = node.next;
		Stack<Integer> reveresedHalf= new  Stack<Integer>();
		
		while(fast != null && fast.next != null){
			reveresedHalf.add(slow.data);
			slow = slow.next;
			fast = fast.next.next;
			
		}
		reveresedHalf.add(slow.data);
		// if size is odd, fast is null
		if(fast == null){
			reveresedHalf .pop();
		}
		fast = slow.next;
		while(fast!=null){
			if(fast.data != reveresedHalf.pop().intValue()){
				return false;
			}
			fast = fast.next;
		}
		
		return true;
	}
}
