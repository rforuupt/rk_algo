package com.rk.linkedlist;

/*
 Given a circular linked list, implement an algorithm that returns the node at the
beginning of the loop.
 */
public class LoopDetection_2_8 {

	
	
	public Node<Integer> getStartOfLoop(Node<Integer> node) {
		if(node == null || node.next==null){
			return null;
		}
		
		Node<Integer> slow = node;
		Node<Integer> fast = node.next;
		
		while(fast!=null && fast.next !=null && slow !=fast){
			slow = slow.next;
			fast = fast.next.next;
		}
		if(slow != fast){
			return null;
		}else{
			slow = node;
			fast = fast.next;
			while(slow !=fast){
				slow = slow.next;
				fast = fast.next;
			}
			return fast;
		}
	}

}
