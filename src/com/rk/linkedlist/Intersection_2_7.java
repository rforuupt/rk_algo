package com.rk.linkedlist;

/*
 Given two (singly) linked lists, determine if the two lists intersect. Return the intersecting
node. Note that the intersection is defined based on reference, not value. That is, if the kth
node of the first linked list is the exact same node (by reference) as the jth node of the second
linked list, then they are intersecting.
 */
public class Intersection_2_7 {

	public static void main(String[] args) {

		Intersection_2_7 test = new Intersection_2_7();
		Node<Integer> first = new Node<Integer>(1);
	    first.appendToTail(1);
	    first.appendToTail(1);

	    Node<Integer> second = new Node<Integer>(2);
	    second.appendToTail(2);
	    second.appendToTail(2);
	    second.appendToTail(2);
	    
	    System.out.println(test.doIntersect(first, second));
	    first.next.next.next = second.next.next.next;
	    System.out.println(test.doIntersect(first, second));
	    
	}
	
	public  boolean doIntersect(Node<Integer> n1, Node<Integer> n2){
		
		Result result1 = getSizeAndLastNode(n1);
		Result result2 = getSizeAndLastNode(n2);
		if( result1 == null || result2==null || result1.node != result2.node ){
			return false;
		}
		
		Node<Integer> fast = null;
		Node<Integer> slow = null;
		
		int diff = result1.size - result2.size;
		if(diff >=0){
			fast = n1;
			slow = n2;
		}else{
			fast = n1;
			slow = n2;
		}
		while(diff >0){
			fast = fast.next;
			diff--;
		}
		
		while(fast != slow || fast !=null){
			fast = fast.next;
			slow = slow.next;
		}
		
		return slow== fast;
		
	}

   public  Result getSizeAndLastNode(Node<Integer> n){
	  
	   if(n== null){
		   return null;
	   }
	   Node<Integer> node = n;
	   int i = 0;
	   while(node.next != null){
		   node = node.next;
		   i++;
	   }
	   
	   return new Result(i, node);
		
	}
   
   private class Result{
	   int size;
	   Node<Integer> node;
	   Result(int size, Node<Integer> node){
		   this.size = size;
		   this.node = node;
	   }
   }
}
