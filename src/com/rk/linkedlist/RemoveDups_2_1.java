package com.rk.linkedlist;

import java.util.HashSet;
import java.util.Set;

/*
 Write code to remove duplicates from an unsorted linked list.
FOLLOW UP
How would you solve this problem if a temporary buffer is not allowed?
 */
public class RemoveDups_2_1 {

	public static void main(String[] args) {
		Node<Character> n = new Node<>('F');
		n.appendToTail('O');
		n.appendToTail('L');
		n.appendToTail('L');
		n.appendToTail('O');
		n.appendToTail('W');
		n.appendToTail('U');
		n.appendToTail('P');
		removeDplicates2(n);
		
		while(n!=null){
			System.out.println(n.data);
			n= n.next;
		}

	}
	
	public static void removeDplicates(Node<Character> node){
		if(node == null || node.next == null){
			return;
		}
		Set<Character> set = new HashSet<>();
		set.add(node.data);
		
		while(node.next !=null){
			if(set.contains(node.next.data)){
				node.next = node.next.next;
			}else{
				set.add(node.next.data);
			}
			node = node.next;
		}
	}
	
	public static void removeDplicates2(Node<Character> node){
		if(node == null || node.next == null){
			return;
		}
		
		Node<Character> slow = node;
		Node<Character> fast = node;

		
		while(slow !=null){
			fast = slow;
			while(fast.next !=null){
				if(slow.data == fast.next.data){
					fast.next = fast.next.next;
				}
				fast = fast.next;
			}
			slow = slow.next;
		}
	}

}
