package com.rk.linkedlist;

/*
You have two numbers represented by a linked list, where each node contains a single
digit.  Write a function that adds the two numbers and returns the sum as a linked list.
*/
public class SumLists_2_5 {

  public static void main(String[] args) {
    Node<Integer> first = new Node<Integer>(1);
    first.appendToTail(1);
    first.appendToTail(1);

    Node<Integer> second = new Node<Integer>(2);
    second.appendToTail(2);
    second.appendToTail(2);
    second.appendToTail(2);

    Node<Integer> result = add(first, second);
    while (result != null) {
      System.out.print(result.data + " ");
      result = result.next;
    }

  }

  private static Node<Integer> add(Node<Integer> first, Node<Integer> second) {
    Node<Integer> n1 = cloneReversed(first);
    Node<Integer> n2 = cloneReversed(second);
    Node<Integer> result = null;
    Node<Integer> temp;
    int carry = 0;
    int sum = 0;
    while (n1 != null || n2 != null) {
      sum = carry;
      if (n1 != null) {
        sum += n1.data;
        n1 = n1.next;
      }
      if (n2 != null) {
        sum += n2.data;
        n2 = n2.next;
      }

      if (sum >= 10) {
        carry = 1;
        sum = sum - 10;
      } else {
        carry = 0;
      }

      if (result == null) {
        result = new Node<Integer>(sum);
      } else {
        temp = new Node<Integer>(sum);
        temp.next = result;
        result = temp;
      }
    }

    return result;

  }

  static Node<Integer> cloneReversed(Node<Integer> node) {
    if (node == null || node.next == null) {
      return node;
    }
    Node<Integer> cloned = new Node<Integer>(node.data);
    node = node.next;
    Node<Integer> temp;
    while (node != null) {
      temp = new Node<Integer>(node.data);
      temp.next = cloned;
      node = node.next;
      cloned = temp;
    }
    return cloned;
  }
}
