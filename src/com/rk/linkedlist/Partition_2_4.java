package com.rk.linkedlist;
/*
 Write code to partition a linked list around a value x, such that all nodes less than x come
before all nodes greater than or equal to x. lf x is contained within the list, the values of x only need
to be after the elements less than x (see below). The partition element x can appear anywhere in the
"right partition"; it does not need to appear between the left and right partitions.
EXAMPLE
Input: 3 -> 5 -> 8 -> 5 -> 10 -> 2 -> 1 [partition = 5)
Output: 3 -> 1 -> 2 -> 10 -> 5 -> 5 -> 8
 */
public class Partition_2_4 {

	public static void main(String[] args) {
		Node<Integer> n = new Node<>(3);
		n.appendToTail(5);
		n.appendToTail(8);
		n.appendToTail(5);
		n.appendToTail(10);
		n.appendToTail(2);
		n.appendToTail(1);
		partition(n,5);
		while(n!=null){
			System.out.println(n.data);
			n = n.next;
		}
	}
	
	public static void partition(Node<Integer> node, int position){
		if(node==null || node.next == null){
			return;
		}
		Node<Integer>  slow = node;
		Node<Integer>  fast = node;
		while(fast!=null && fast.data < position){
			fast = fast.next;
		}
		
		while(fast !=null){
			while(slow.data < position){
				slow = slow.next;
			}
			while(fast.next !=null && fast.data >= position){
				fast = fast.next;
			}
			if(fast.data > position)
				return;
			Integer temp = slow.data;
			slow.data = fast.data;
			fast.data = temp;
		}
	}

}
