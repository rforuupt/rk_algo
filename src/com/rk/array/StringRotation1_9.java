package com.rk.array;

/* Assume you have a method isSubstring which checks if one word is a substring of another. 
 * Given two strings, S1 and S2, write code to check if S2 is a rotation of S1 using only one call to isSubstring 
 * (e.g.,"waterbottle"is a rotation of"erbottlewat").
 * 
 */
public class StringRotation1_9 {

	public static void main(String[] args) {
		System.out.println(isRotation("waterbottle","erbottlewat"));
		System.out.println(isRotation("waterbottle","erbottlewatwfrgdsadsf"));
	}
	public static boolean isRotation(String s1, String s2) {
		return isSubstring(s1, s2 + s2);
	}
	public static boolean isSubstring(String s1, String s2) {
		return s2.contains(s1);
	}

}
