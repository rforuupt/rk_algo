package com.rk.array;

/*There are three types of edits that can be performed on strings: insert a character,
remove a character, or replace a character. Given two strings, write a function to check if they are
one edit (or zero edits) away.
 */
public class OneAway1_5 {

	public static void main(String[] args) {
		System.out.println(isOneEdit("pale","bake"));
	}
	
	public static boolean isOneEdit(String s1, String s2) {
		if(s1.length() == s2.length()){
			return sameLength(s1, s2);
		}else{
			if(Math.abs(s1.length() - s2.length()) >1)
				return false;
			if((s1.length() - s2.length()) > 0){
				return diffLenght(s1, s2);
			}else{
				return diffLenght(s2, s1);
			}
		}
		
	}
	
	public static boolean sameLength(String s1, String s2) {
		int diff = 0;
		for(int i=0; i<s1.length(); i++){
			if(s1.charAt(i) != s2.charAt(i)){
				diff++;
			}
			if(diff>1)
				return false;
		}
		return true;
	}
	
	public static boolean diffLenght(String s1, String s2) {
		int diff = 0;
		int j=0;
		int i=0;
		for(; i<s1.length() && j<s2.length(); i++){
			if(s1.charAt(i) != s2.charAt(j)){
				diff++;
			}else{
				j++;
			}
			if(diff>1)
				return false;
		}
		return diff ==0 && i== s1.length() -1;
	}

}
