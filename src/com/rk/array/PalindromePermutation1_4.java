package com.rk.array;

import java.util.Arrays;

/* Given a string, write a function to check if it is a permutation of a palindrome.
A palindrome is a word or phrase that is the same forwards and backwards. A permutation
is a rea rrangement of letters. The palindrome does not need to be limited to just dictionary words. */

public class PalindromePermutation1_4 {

	public static void main(String[] args) {
		System.out.println(isPalintromic("tactcoapapa"));
	}
	
	public static boolean isPalintromic(String args) {
		char [] carr = args.toCharArray();
		Arrays.sort(carr);
		boolean isOdd = true;
		int oddCount =0;
		for(int i=1; i<carr.length; i++){
			if(carr[i] == carr[i-1]){
				isOdd = !isOdd;
			}else{
				if(isOdd){
					oddCount++;
				}
				if(oddCount >1){
					return false;
				}
				isOdd = true;
			}
		}
		if(oddCount ==1 && isOdd){
			return false;
		}
		return true;
	}

}
