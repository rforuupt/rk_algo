package com.rk.array;

import java.util.HashMap;
import java.util.Map;

/*Is Unique: Implement an algorithm to determine if a string has all unique characters. What if you
cannot use additional data structures?*/

public class UniqueCharacters1_1 {

	public static void main(String[] args) {
		System.out.println(hasAllUniqueCharcters2("abcdb"));

	}
	// Use Map
	public static boolean hasAllUniqueCharcters(String args) {
		Map<Character, Character> charMap = new HashMap<>();
		for(int i=0; i<args.length(); i++){
			if(charMap.containsKey(args.charAt(i))){
				return false;
			}
			
			charMap.put(args.charAt(i), args.charAt(i));
		}
		
		return true;

	}
	
	// Use array with index
	public static boolean hasAllUniqueCharcters2(String args) {
		byte [] s = new byte [26] ;
		char a = 'a';
		char b = 'b';
		
		for(int i=0; i<args.length(); i++){
			b = args.charAt(i);
			
			if(s[b-a] >0){
				return false;
			}else{
				s[b-a] = 1;
			}
		}
		
		return true;

	}

}
