package com.rk.array;

import java.util.Arrays;

/* Given two strings, write a method to decide if one is a permutation of the
other.*/

public class CheckPermutation1_2 {

	public static void main(String[] args) {
		
		System.out.println(isPermuation2("abcd", "bcda"));
	}
	
	public static boolean isPermuation(String s1, String s2) {
		if(s1.length() != s2.length()){
			return false;
		}
		
		byte [] s1Arr = new byte [26] ;
		byte [] s2Arr = new byte [26] ;
		char a = 'a';
		char b = 'b';
		
		for(int i=0; i<s1.length(); i++){
			b = s1.charAt(i);
			s1Arr[b-a] ++ ;
		}
		
		for(int i=0; i<s2.length(); i++){
			b = s2.charAt(i);
			s2Arr[b-a] ++ ;
		}
		
		for(int i=0; i<s2.length(); i++){
			if( s1Arr[i] != s2Arr[i]){
				return false;
			}
		}
		return true;

	}
	
	public static boolean isPermuation2(String s1, String s2) {
		if(s1.length() != s2.length()){
			return false;
		}
		
		char [] s1Arr = s1.toCharArray() ;
		Arrays.sort(s1Arr, 0, s1.length()); 
		
		char [] s2Arr = s2.toCharArray() ;
		Arrays.sort(s2Arr, 0, s2.length()); 
		
		for(int i=0; i<s2Arr.length; i++){
			if( s1Arr[i] != s2Arr[i]){
				return false;
			}
		}
		return true;

	}

}
