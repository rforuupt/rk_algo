package com.rk.array;
/*
 * Write an algorithm such that if an element in an MxN matrix is 0, its entire row and column are set to O.
 */

public class ZeroMatrix1_8 {

	public static void main(String[] args) {

		int m = 4;
		int n = 5;
		int[][] input = new int [m][n];
		int count = 1;
		for(int i=0; i<m ; i++){
			for(int j= 0; j< n ; j++){
				input[i][j] = count++ ;
			}
		}
		
		for(int i=0; i<m ; i++){
			for(int j= 0; j< n ; j++){
				System.out.print(input[i][j] +"\t"); 
			}
			System.out.println("");
		}
		
		System.out.println("****************************");
		input[2][3] =0;
		setZeros(input);
		
		for(int i=0; i<m ; i++){
			for(int j= 0; j< n ; j++){
				System.out.print(input[i][j] +"\t"); 
			}
			System.out.println("");
		}
		
	}
	public static void setZeros(int[][] matrix) {
		int m= matrix.length;
		if(m==0)
			return;
		
		int n= matrix[0].length;
		
		for(int i=0; i<m ; i++){
			for(int j= 0; j< n ; j++){
				if(matrix[i][j] == 0){
					matrix[i][0] = 0;
					matrix[0][j] = 0;
				}
			}
		}
		
		for(int i=0; i<m ; i++){
			for(int j= 0; j< n ; j++){
				if(matrix[i][0] == 0 || matrix[0][j] == 0){
					matrix[i][j] = 0 ;
				}
			}
		}
		
	}

}
