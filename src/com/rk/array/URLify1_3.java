package com.rk.array;

/*
Write a method to replace all spaces in a string with '%20: You may assume that the string
has sufficient space at the end to hold the additional characters, and that you are given the "true"
length of the string. (Note: If implementing in Java, please use a character array so that you can
perform this operation in place.)
EXAMPLE
Input: "Mr John Smith "; 13
Output: "Mr%20J ohn%20Smith"

*/
public class URLify1_3 {

  public static void main(String[] args) {
    System.out.println(urlify("Mr John Smith".toCharArray()));
  }

  public static String urlify(char[] args) {

    int length = args.length;
    int whitespaces = 0;
    for (int i = 0; i < length; i++) {
      if (' ' == args[i]) {
        whitespaces++;
      }
    }

    char[] urlifyCharArr = new char[length + 2 * whitespaces];
    int i = 0;
    int j = 0;
    while (i < length) {
      if (' ' == args[i]) {
        urlifyCharArr[j++] = '%';
        urlifyCharArr[j++] = '2';
        urlifyCharArr[j++] = '0';
        i++;
      } else {
        urlifyCharArr[j++] = args[i++];
      }
    }
    return String.valueOf(urlifyCharArr);
  }
}
