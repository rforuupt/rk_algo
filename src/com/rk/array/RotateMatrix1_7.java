package com.rk.array;

/* Given an image represented by an NxN matrix, where each pixel in the image is 4
bytes, write a method to rotate the image by 90 degrees. Can you do this in place?
*/
public class RotateMatrix1_7 {

	public static void main(String[] args) {
		int n = 5;
		int[][] input = new int [n][n];
		int count = 1;
		for(int i=0; i<n ; i++){
			for(int j= 0; j< n ; j++){
				input[i][j] = count++ ;
			}
		}
		for(int i=0; i<n ; i++){
			for(int j= 0; j< n ; j++){
				System.out.print(input[i][j] +"\t"); 
//				System.out.print(i+" "+j +"\t");
			}
			System.out.println("");
		}
		System.out.println("************************************");
		rotate(input);
		for(int i=0; i<n ; i++){
			for(int j= 0; j< n ; j++){
				System.out.print(input[i][j] +"\t"); 
//				System.out.print(i+" "+j +"\t");
			}
			System.out.println();
		}
	}
	public static void rotate(int[][] matrix) {
		int n = matrix.length;
		for (int layer = 0; layer < n / 2; ++layer) {
			
			int st = layer; 
			int last=n-1-layer;
			for(int i =  st; i < last; ++i) {
				int ofset = i - st;
				// save top
				int top = matrix[ st][i]; 
				// left -> top
				matrix[ st][i] = matrix[last -ofset][ st];
				// bottom -> left
				matrix[last -ofset][ st] = matrix[last][last -ofset];
				// right -> bottom
				matrix[last][last -ofset] = matrix[i][last];
				// top -> right
				matrix[i][last] = top; // right <- saved top }
			}
		}
	}
	
}
