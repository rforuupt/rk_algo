package com.rk.array;


/*Implement a method to perform basic string compression using the counts
of repeated characters. For example, the string aabcccccaaa would become a2b1c5a3. If the
"compressed" string would not become smaller than the original string, your method should return
the original string. You can assume the string has only uppercase and lowercase letters (a - z).*/
public class StringCompression1_6 {

	public static void main(String[] args) {
		System.out.println(getCompressed("aabcccccaaa"));
	}

	public static String getCompressed(String args) {

		char [] compressedArr = new char[args.length()];
		int count =1;
		int index = 0;
		
		for(int i=1; i<args.length(); i++){
			if(args.charAt(i)== args.charAt(i-1)){
				count ++ ;
			}else{
				if(index >= args.length() -2){
					return args;
				}
				compressedArr[index] = args.charAt(i-1);
				compressedArr[index+1] = (char)('0'+count);
				count = 1;
				index = index + 2;
			}
		}
		compressedArr[index] = args.charAt(args.length()-1);
		compressedArr[index+1] = (char)('0'+count);
		
		String str = String.valueOf(compressedArr).trim();
		return str.length() < args.length() ? str:args;

	}
}
