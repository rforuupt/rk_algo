package com.rk.stackQueue;

import java.util.Iterator;
import java.util.LinkedList;

/*
 * An animal shelter, which holds only dogs and cats, operates on a strictly"first in, first out" basis. 
 * People must adopt either the "oldest" (based on arrival time) of all animals at the shelter, 
 * or they can select whether they would prefer a dog or a cat (and will receive the oldest animal of that type). 
 * They cannot select which specific animal they would like. Create the data structures to maintain this system and 
 * implement operations such as enqueue, dequeueAny, dequeueDog, and dequeueCat. You may use the built-in Linked List data structure.
 */
public class AnimalShelter_3_6 {

	LinkedList<String> ll = new LinkedList<>();
	public void enqueue(String element) {
		ll.add(element);
	}
	public String dequeueAny() {
		return ll.removeFirst();
	}
	public String dequeueCat() {
		Iterator<String> iterator = ll.iterator();
		String animal="";
		while(iterator.hasNext()){
			animal = iterator.next();
			if(animal.startsWith("cat")){
				iterator.remove();
				return animal;
			}
		}
		return null;
	}
	public String dequeueDog() {
		Iterator<String> iterator = ll.iterator();
		String animal="";
		while(iterator.hasNext()){
			animal = iterator.next();
			if(animal.startsWith("dog")){
				iterator.remove();
				return animal;
			}
		}
		return null;
	}

}
