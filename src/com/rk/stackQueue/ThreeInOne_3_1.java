package com.rk.stackQueue;

import java.util.ArrayList;
import java.util.List;

/*
 *Describe how you could use a single array to implement three stacks.
 */
public class ThreeInOne_3_1 {

	private int size;
	private int capacity;
	int arr[];
	int stack1TopIndex;
	int stack3TopIndex;
	int stack2StartIndex;
	int stack2EndIndex;
	
	public ThreeInOne_3_1(int capacity){
		this.capacity = capacity;
		arr = new int[capacity];
		stack3TopIndex = capacity -1; 
	}
	public void push(int stackNum, int value) throws StackOperationException{
		if(size == capacity){
			throw new StackOperationException("Stack is Full");
		}
		
		size++;
		if(stackNum==1){
			arr[stack1TopIndex ++] = value;
				
		}else if(stackNum==2){
			arr[stack2EndIndex ++] = value;
				
		}else if(stackNum==3){
		  arr[stack3TopIndex --] = value;
		}
	}
	public int pop(int i) throws StackOperationException {
		if(i==1){
			if(stack1TopIndex == 0)
				throw new StackOperationException("Stack is Empty");
			size --;
			return arr[--stack1TopIndex];
				
		}else if(i==2){
			if(stack2StartIndex == stack2EndIndex)
				throw new StackOperationException("Stack is Empty");
			size --;
			return arr[--stack2EndIndex];
				
		}else if(i==3){
			if(stack3TopIndex == capacity -1)
				throw new StackOperationException("Stack is Empty");
			size --;
		    return arr[++stack3TopIndex];
			
		}
		throw new StackOperationException("Invalid Stack Number");
	} 
	
	public static class StackOperationException extends Exception {

		StackOperationException(String message){
			super(message);
		}
	}
}
