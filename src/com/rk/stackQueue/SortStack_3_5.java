package com.rk.stackQueue;

import java.util.Stack;

/*
 * Write a program to sort a stack such that the smallest items are on the top. You can use an additional temporary stack,
 * but you may not copy the elements into any other data structure (such as an array). 
 * The stack supports the following operations: push, pop, peek, and isEmpty.
 */
public class SortStack_3_5 {

	Stack<Integer> sotredStack = new Stack<>();
	Stack<Integer> temp = new Stack<>();
	public void push(int i) {
		while(!sotredStack.isEmpty() && sotredStack.peek()<i){
			temp.push(sotredStack.pop());
		}
		sotredStack.push(i);
		while(!temp.empty()){
			sotredStack.push(temp.pop());
		}
	}
	public int pop() {
		if(!sotredStack.isEmpty()){
			return sotredStack.pop();
		}
		return 0;
	}
	
}
