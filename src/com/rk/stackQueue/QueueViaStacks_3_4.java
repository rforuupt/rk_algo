package com.rk.stackQueue;

import java.util.Stack;

/*
 * Implement a MyQueue class which implements a queue using two stacks.
 */
public class QueueViaStacks_3_4 {

	Stack<Integer> enqueStack = new Stack<>();
	Stack<Integer> dequeStack = new Stack<>();
	public void enque(int i) {
		enqueStack.push(i);
	}

	public int dequeue() {
		if(dequeStack.isEmpty() && !enqueStack.isEmpty() ){
			while(!enqueStack.isEmpty()){
				dequeStack.push(enqueStack.pop());
			}
		}
		return dequeStack.pop();
	}

}
