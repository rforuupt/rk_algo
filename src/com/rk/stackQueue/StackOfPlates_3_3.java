package com.rk.stackQueue;

import java.util.ArrayList;
import java.util.List;

/*
 *Imagine a (literal) stack of plates. If the stack gets too high, it might topple.
Therefore, in real life, we would likely start a new stack when the previous stack exceeds some
threshold. Implement a data structure SetOfStacks that mimics this. SetOfStacks should be
composed of several stacks and should create a new stack once the previous one exceeds capacity.
SetOfStacks. push () and SetOfStacks. pop () should behave identically to a single stack
(that is, pop ( ) should return the same values as it would if there were just a single stack).

FOLLOW UP
Implement a function popAt (int index) which performs a pop operation on a specific sub-stack.
 */
public class StackOfPlates_3_3 {
	int index =0;
	List<int[]> stackList;
	List<Integer> indexList = new ArrayList<>();
	
	public StackOfPlates_3_3(){
		stackList = new ArrayList<int[]>();
		stackList.add(new int[10]);
	}

	public void push(int i) {
		if(stackList.get(stackList.size() -1)[9] !=0){
			stackList.add(new int[10]);
			index =0;
			indexList.add(10);
		}
		stackList.get(stackList.size() -1)[index++] =i;		
	}

	public int pop() {
		return stackList.get(stackList.size() -1)[--index];
	}

	public int pop(int i) {
		int currIndex = indexList.get(i-1);
		indexList.set(i-1, currIndex -1);
		return stackList.get(i -1)[currIndex -1];
		
	}
	
}
