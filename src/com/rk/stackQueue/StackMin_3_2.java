package com.rk.stackQueue;

import java.util.Stack;

/*
 How would you design a stack which, in addition to push and pop, has a function min
which returns the minimum element? Push, pop and min should all operate in 0(1) time.
 */
public class StackMin_3_2 {

	private int [] arr = new int[10];
	private int index = 0;
	private Stack<Integer> stack = new Stack<Integer>();
	public void push(int i) {
		arr[ index++] = i;
		if(stack.isEmpty() || stack.peek()>=i){
			stack.push(i);
		}
	}
	public int peek() {
		return arr[ index -1];
	}
	public int pop() {
		if(peek()<= stack.peek()){
			stack.pop();
		}
		return arr[ --index];
	}
	public int min() {
		return stack.peek();
	}

}
