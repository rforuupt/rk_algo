package com.rk.graph;

public class BSTNode {
	public int value;
	public BSTNode left;
	public BSTNode right;
	boolean visited = false;
	BSTNode(int value){
		this.value = value;
	}
}
