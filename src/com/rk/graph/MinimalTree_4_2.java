package com.rk.graph;

import java.util.Arrays;

/*
 * Given a sorted (increasing order) array with unique integer elements, write an
algorithm to create a binary search tree with minimal height.
 */
public class MinimalTree_4_2 {

	public BSTNode createMinimalTree(int[] arr) {
		BSTNode root = null;
		
		if(arr.length >0){
			root = new BSTNode(arr[arr.length/2]);
		}
		
		if(arr.length >1){
		root.left = createMinimalTree(Arrays.copyOfRange(arr, 0, arr.length/2));
		}
		if(arr.length >2){
			root.right = createMinimalTree(Arrays.copyOfRange(arr, arr.length/2 +1, arr.length));
		}
		
		return root;
	}

}
