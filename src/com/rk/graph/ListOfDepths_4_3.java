package com.rk.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
 * Given a binary tree, design an algorithm which creates a linked list of all the nodes
at each depth (e.g., if you have a tree with depth D, you'll have D linked lists).
 */
public class ListOfDepths_4_3 {

	public List<LinkedList<BSTNode>> getListOfDepths(BSTNode root) {
		List<LinkedList<BSTNode>> llList = new ArrayList<>();
		LinkedList<BSTNode> queue = new LinkedList<>();
		LinkedList<BSTNode> depthList = new LinkedList<>();
		BSTNode node;
		if(root != null){
			queue.offer(root);
			queue.offer(new BSTNode(-1));
			while(!queue.isEmpty()){
				node =queue.remove();
				if(node.value == -1){
					llList.add(depthList);
					depthList = new LinkedList<>(); 
					if(!queue.isEmpty()){
						queue.offer(node);
					}
					
				}else{
					depthList.add(node);
					if( node.left != null){
						queue.offer(node.left);
					}
					if(node.right !=null){
						queue.offer(node.right);
					}
					
				}
				
			}
		}
		
		return llList;
	}

}
