package com.rk.graph;

import java.util.ArrayList;
import java.util.List;

public class Node {
	public String name;
	public List<Node> children;
	boolean visited = false;
	Node(String name){
		this.name = name;
		this.children = new ArrayList<>();
	}
}
