package com.rk.graph;

import java.util.LinkedList;

/*
 * Given a directed graph, design an algorithm to find out whether there is a
route between two nodes.
 */
public class RouteBetweenNodes_4_1 {

	public boolean hasRoute(Node node, Node node2) {
		if(node == null || node2==null){
			return false;
		}
		
		LinkedList<Node> queue = new LinkedList<>();
		node.visited = true;
		queue.offer(node); //Add to the end of queue
		
		 while (!queue.isEmpty()) {
			 Node r = queue.remove(); //II Remove from the front of the queue
			 if(r==node2){
				 return true;
			 }
			 for (Node n :r.children) {
				 if (n.visited == false) {
					 n.visited = true;
					 queue.offer(n);
				 }
			 }
		 }
		return false;
	}

}
